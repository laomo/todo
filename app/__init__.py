from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.openid import OpenID
from config import basedir
import os

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

import auth.models
auth.models.init_app(app)
oid = OpenID(app, os.path.join(basedir, 'tmp'))
import auth.views
app.register_blueprint(auth.views.auth, url_prefix = '/auth')

from app import views, models
