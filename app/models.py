from app import db

class Todo(db.Model):
    id =db.Column(db.Integer, primary_key = True)
    timestamp = db.Column(db.DateTime)
    time_str = db.Column(db.String(64))
    repeat_type = db.Column(db.Integer)
    content = db.Column(db.String(140))

    def __repr__(self):
        return '<Todo %r>' % (self.content)
