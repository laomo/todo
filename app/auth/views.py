from flask import render_template, flash, redirect, session, url_for, request, g
from flask.ext.login import login_user, logout_user, current_user, login_required
from app import db, oid
from forms import LoginForm
from models import User, ROLE_USER, ROLE_ADMIN
from datetime import datetime
import config

from flask import Blueprint
auth = Blueprint('auth', __name__, template_folder="templates")

@auth.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@auth.route('/login', methods = ['GET', 'POST'])
@oid.loginhandler
def login():
    form = LoginForm()
    #if form.validate_on_submit():
    session['remember_me'] = True
    print config.google_openid
    return oid.try_login(config.google_openid, ask_for = ['nickname', 'email'])

@oid.after_login
def after_login(resp):
    if resp.email is None or resp.email == "":
        flash('Invalid login. Please try again.')
        return redirect(url_for('login'))
    user = User.query.filter_by(email = resp.email).first()
    if user is None:
        nickname = resp.nickname
        if nickname is None or nickname == "":
            nickname = resp.email.split('@')[0]
        user = User(nickname = nickname, email = resp.email, role = ROLE_USER)
        db.session.add(user)
        db.session.commit()
    remember_me = False
    if 'remember_me' in session:
        remember_me = session['remember_me']
        session.pop('remember_me', None)
    login_user(user, remember = remember_me)
    return redirect(request.args.get('next') or url_for('index'))


@auth.before_request
def before_request():
    g.user = current_user
    if g.user.is_authenticated():
        g.user.last_seen = datetime.utcnow()
        db.session.add(g.user)
        db.session.commit()
