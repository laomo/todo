from hashlib import md5
from app import db
from flask.ext.login import LoginManager


ROLE_USER = 0
ROLE_ADMIN = 1

__all__ = ('User')

class User(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    nickname = db.Column(db.String(64), index = True, unique = True)
    email = db.Column(db.String(120), index = True, unique = True)
    role = db.Column(db.SmallInteger, default = ROLE_USER)
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime)

    def avatar(self, size):
        return 'http://www.gravatar.com/avatar/' + md5(self.email.lower()).hexdigest()+ '?s=' + str(size)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % (self.nickname)


def init_app(app):
    lm = LoginManager()
    lm.init_app(app)
    lm.user_loader(load_user)
    lm.login_view = "/auth/login"

def load_user(id):
    return User.query.get(int(id))
