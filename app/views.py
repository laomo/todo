from flask import render_template, flash, redirect, session, url_for, request, g
from flask.ext.login import login_user, logout_user, current_user, login_required
from app import app, db
from forms import LoginForm, EditForm
from auth.models import User, ROLE_USER, ROLE_ADMIN
from datetime import datetime

@app.route('/')
@app.route('/index')
def index():
    user = { 'nickname': 'Miguel' } # fake user
    posts = [ # fake array of posts
             {
                 'author': { 'nickname': 'John' },
                 'body': 'Beautiful day in Portland!'
             },
             {
                 'author': { 'nickname': 'Susan' },
                 'body': 'The Avengers movie was so cool!'
             }
             ]
    return render_template("index.html",
                           title = 'Home',
                           user = user,
                           posts = posts, form = LoginForm())
